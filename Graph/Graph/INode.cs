using System;
using System.Collections.Generic;

namespace Graph
{
    public interface INode<T> where T : IEquatable<T>
    {
        int Id { get; set; }
        T Data { get; set; }
    }
}