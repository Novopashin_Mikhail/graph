using System;

namespace Graph
{
    public class DijkstraData<T> where T : IEquatable<T>
    {
        public double Price { get; set; }
        public Graph<T>.Node<T> Previous { get; set; }
    }
}