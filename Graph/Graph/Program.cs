﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.Configuration;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace Graph
{
    class Program
    {
        static void Main(string[] args)
        {
            var roadMap = new RoadMap<City>();
            var pathToFile = GetPathToFile();

            HSSFWorkbook excelWorkBook;
            using (var file = new FileStream(pathToFile, FileMode.Open, FileAccess.Read))
            {
                excelWorkBook= new HSSFWorkbook(file);
            }
            
            ISheet sheet = excelWorkBook.GetSheet("Sheet1");
            FillGraph(sheet, roadMap);

            var city1 = new City {Name = "Москва"};
            var city2 = new City {Name = "Невинномысск"};
            var result = roadMap.RunDijkstra(city1, city2);
            ShowResult(result);
        }

        private static void ShowResult(List<Graph<City>.Node<City>> result)
        {
            foreach (var item in result)
            {
                Console.WriteLine(item.Data.Name);
            }
        }

        private static void FillGraph(ISheet sheet, RoadMap<City> roadMap)
        {
            for (int row = 1; row <= sheet.LastRowNum; row++)
            {
                var iRow = sheet.GetRow(row);
                roadMap.AddNode(new City() {Name = iRow.GetCell(0).StringCellValue});
            }

            for (int row = 1; row <= sheet.LastRowNum; row++)
            {
                var iRow = sheet.GetRow(row);
                for (int column = 1; column < iRow.LastCellNum; column++)
                {
                    var iColumn = iRow.GetCell(column);
                    if (iColumn == null || CheckIfString(iColumn)) continue;
                    var weight = (int) iColumn.NumericCellValue;
                    if (weight <= 500)
                        roadMap.AddNeighbor(row, column, weight);
                }
            }
        }

        private static string GetPathToFile()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            var builder = new ConfigurationBuilder()
                .SetBasePath(currentDirectory)
                .AddJsonFile("appSettings.json");

            var config = builder.Build();
            return config["pathToFile"];
        }

        static bool CheckIfString(ICell iColumn)
        {
            return iColumn.CellType == CellType.String &&
                   (string.IsNullOrEmpty(iColumn.StringCellValue) || iColumn.StringCellValue == " ");
        }
    }
}