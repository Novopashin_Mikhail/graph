using System;
using System.Collections.Generic;
using System.Linq;

namespace Graph
{

    public class Graph<T>
        where T: IEquatable<T>
    {
        public int Count => NodeList.Count;
        public int MaxId => NodeList.GetMax(x => x.Id)?.Id ?? 0;
        protected List<Node<T>> NodeList { get; private set; } = new List<Node<T>>();
        protected int GetIdByData(T data)
        {
            var node = NodeList.FirstOrDefault(x => x.Data.Equals(data));
            return node?.Id ?? default;
        }

        public Graph<T> CreateGraph<T>() where T: IEquatable<T>
        {
            return new Graph<T>();
        }

        //1
        public void AddNode(T data)
        {
            var node = new Node<T>(MaxId + 1, data);
            NodeList.Add(node);
        }
        //1
        public void AddNodes(IEnumerable<T> dataRange)
        {
            var nodeRange = dataRange.Select(data => new Node<T>(Count + 1, data));
            NodeList.AddRange(nodeRange);
        }
        //2
        public bool Contains(T item)
        {
            return NodeList.Any(node => node.Data.Equals(item));
        }
        //2
        public bool Contains(int id)
        {
            return NodeList.Any(node => node.Id == id);
        }
        //3
        public void RemoveNode(T data)
        {
            var findNode = NodeList.FirstOrDefault(node => node.Data.Equals(data));
            if (findNode == null) return;
            NodeList.Remove(findNode);
        }
        //3
        public void RemoveNodes(IEnumerable<T> dataRange)
        {
            foreach (var data in dataRange)
            {
                RemoveNode(data);
            }
        }
        //4
        public Node<T> GetNode(int id)
        {
            var node = NodeList.FirstOrDefault(n => n.Id == id);
            return node;
        }
        //5
        public bool TryAddNode(int id)
        {
            if (Contains(id))
            {
                return false;
            }
            var node = new Node<T>(id, default);
            NodeList.Add(node);
            return true;
        }

        public bool AddNeighbor(int nodeId, int neigborId, int weight)
        {
            var node = GetNode(nodeId);
            return node.TryAddNeighbor(neigborId, weight);
        }
        
        public class Node<T> : INode<T> where T : IEquatable<T>
        {
            public int Id { get; set; }
            public T Data { get; set; }
            
            public Node(){}
            
            private IList<Edge> Neighbors { get; set; }
            
            public Node(int id, T data)
            {
                Id = id;
                Data = data;
                Neighbors = new List<Edge>();
            }

            public class Edge
            {
                public int Id { get; set; }
                public int Weight { get; set; }
            }

            public bool TryAddNeighbor(int id, int weight)
            {
                if (Neighbors.Any(x => x.Id == id) || weight == 0) return false;
                Neighbors.Add(new Edge {Id = id, Weight = weight});
                return true;
            }

            public IEnumerable<Edge> GetNeighbors()
            {
                return Neighbors;
            }            
        }
    }
}