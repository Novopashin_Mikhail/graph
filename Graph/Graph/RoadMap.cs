using System;
using System.Collections.Generic;

namespace Graph
{
    public class RoadMap<T> : Graph<T> where T : IEquatable<T>
    {
        public List<Node<T>> RunDijkstra(T start, T end)
        {
            var startId = GetIdByData(start);
            var endId = GetIdByData(end);
            return RunDijkstra(startId, endId);
        }

        public List<Node<T>> RunDijkstra(int startId, int endId)
        {
            var start = GetNode(startId);
            var end = GetNode(endId);
            
            List<Node<T>> notVisited = new List<Node<T>>();
            notVisited.AddRange(NodeList);
            var track = new Dictionary<Node<T>, DijkstraData<T>>();
            track[start] = new DijkstraData<T> {Previous = null, Price = 0};

            while (true)
            {
                Node<T> toOpen = null;
                double bestPrice = double.PositiveInfinity;
                foreach (var v in notVisited)
                {
                    if (track.ContainsKey(v) && track[v].Price < bestPrice)
                    {
                        toOpen = v;
                        bestPrice = track[v].Price;
                    }
                }

                if (toOpen == null) return null;
                if (toOpen == end) break;
                foreach (var neighbor in toOpen.GetNeighbors())
                {
                    var currentPrice = track[toOpen].Price + neighbor.Weight;
                    var nextNode = GetNode(neighbor.Id);
                    if (!track.ContainsKey(nextNode) || track[nextNode].Price > currentPrice)
                        track[nextNode] = new DijkstraData<T>{Price = currentPrice, Previous = toOpen};
                }
                notVisited.Remove(toOpen);
            }
            var result = new List<Node<T>>();
            while (end != null)
            {
                result.Add(end);
                end = track[end].Previous;
            }
            result.Reverse();
            return result;
        }
    }
}